#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include "ros_tutorial_action/FibonacciAction.h"

class FibonacciAction {
    
    protected:
    
    // Node Handle declaration
    ros::NodeHandle nh_;
    
    // Action server declaration
    actionlib::SimpleActionServer<ros_tutorial_action::FibonacciAction> as_;

    std::string action_name_;

    // Declare feedback and result
    ros_tutorial_action::FibonacciFeedback feedback_;
    ros_tutorial_action::FibonacciResult result_;

    public:

    // Initialise action secrver (node handle, action name, action callback function)

    FibonacciAction(std::string name) : as_(nh_, name, boost::bind(&FibonacciAction::executeCB, this, _1), false), action_name_(name) {
        as_.start();
    }

    ~FibonacciAction(void) {

    }

    /* This function receives an action goal message and performs a specified action
        (here, a Fibonacci calculation)
    */

   void executeCB(const ros_tutorial_action::FibonacciGoalConstPtr& goal) {
       ros::Rate r(1); // Loop rate : 1Hz
       bool success = true;

       // Setting  Fibonacci sequence initialisation
       // create first and second message initialization
       feedback_.sequence.clear();
       feedback_.sequence.push_back(0);
       feedback_.sequence.push_back(1);

       // Notify the user of action name, goal, initial two values of Fibonacci sequence
       ROS_INFO("%s: Executing, creating fibonacci sequence of order %i with seeds %i, %i",
        action_name_.c_str(), goal->order, feedback_.sequence[0], feedback_.sequence[1]);
       
       // Action content
       for(int i=1; i<=goal->order; i++) {
           // If cancelled, confirm to user
           if (as_.isPreemptRequested() || !ros::ok()) {
               ROS_INFO("%s: Preempted", action_name_.c_str());
               as_.setPreempted();
               success = false;
               break;
           }
           /* Calculate next Fibonacci value and store it in the feedback
            Value = current value + previous value
           */
            feedback_.sequence.push_back(feedback_.sequence[i] + feedback_.sequence[i-1]);
            as_.publishFeedback(feedback_);
            r.sleep();
        }

        // If target reached, publish result

        if (success) {
            result_.sequence = feedback_.sequence;
            ROS_INFO("%s: succeded", action_name_.c_str());
            as_.setSucceeded(result_);
        }

   }

};

int main(int argc, char** argv) {

    ros::init(argc, argv, "action_server");

    FibonacciAction fibonacci("ros_tutorial_action");
    ros::spin();

    return 0;
}