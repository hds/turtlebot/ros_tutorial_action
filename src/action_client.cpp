#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include "ros_tutorial_action/FibonacciAction.h"

int main(int argc, char** argv) {

    ros::init(argc, argv, "action_client");

    // Action client declaration
    // Action name : ros_tutorial_action
    actionlib::SimpleActionClient<ros_tutorial_action::FibonacciAction> ac("ros_tutorial_action", true);

    ROS_INFO("Waiting for action server to start");
    ac.waitForServer();

    ROS_INFO("Action server started, sending goal");
    ros_tutorial_action::FibonacciGoal goal;
    goal.order = 20;
    ac.sendGoal(goal);

    // Set a timeout
    bool finished_before_timeout = ac.waitForResult(ros::Duration(30.0));
    if (finished_before_timeout) {
        actionlib::SimpleClientGoalState state = ac.getState();
        ROS_INFO("Action finished %s, value : %d", state.toString().c_str());
    }
    else {
        ROS_ERROR("Action did not finis before timeout");
    }

    return 0;
}